import 'package:flutter/material.dart';

void main() {
  runApp(WeatherPage());
}

class WeatherPage extends StatelessWidget {

  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),

      ),
    );
  }
}

AppBar buildAppBarWidget() {
  return AppBar(
    backgroundColor: Colors.transparent,
    actions: <Widget>[
      IconButton(
        onPressed: () {},
        icon: Icon(
          Icons.business,
          color: Colors.white,
        ),
      ),
      IconButton(
          onPressed: () {},
          icon: Icon(
            Icons.more_vert_outlined,
            color: Colors.white,
          ))
    ],

  );
}

Widget buildBodyWidget() {
  return Column(
    children: <Widget>[
      Column(
        children: <Widget>[
          Container(width: double.infinity,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: NetworkImage("https://www.gannett-cdn.com/-mm-/481602f6d9edcd4edf3b3b8a2ff6d2d98ae8eb24/c=0-0-507-286/local/-/media/MIGroup/PortHuron/2014/09/16/1410873237000-SUNNY.jpg?width=507&height=286&fit=crop&format=pjpg&auto=webp"),
                fit: BoxFit.cover,
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.fromLTRB(20, 66, 0, 180),
                  child :Text("Pattaya",
                    style: TextStyle(fontSize: 50,
                        color: Colors.white),
                  ),
                ),
                infoWeather(),
                weatherItems(),
                Divider(
                  color: Colors.grey,
                ),
                TodayListTile(),
                TomorrowListTile(),
                ThursdayListTile(),
                FridayListTile(),
                SaturdayListTile(),
                SundayListTile(),
              ],
            ),
          ),
        ],
      ),
    ],
  );
}

Widget infoWeather() {
  return Column(
    children: <Widget>[
      Row(
        crossAxisAlignment: CrossAxisAlignment.baseline,
        textBaseline: TextBaseline.alphabetic,
        children: [
          Text(" 27°C",
            style: TextStyle(
              color: Colors.white,
              fontSize: 70,
              fontWeight: FontWeight.bold,
            ),
          ),
          Text(
            " Mostly Sunny",
            style: TextStyle(
                color: Colors.white,
                fontSize: 20
            ),
          ),
        ],
      ),
      Align(
        alignment: Alignment.topLeft,
        child: Text("  Jan 3 Tue 31°C / 22°C ",
            style: TextStyle(
                fontSize: 20,
                color: Colors.white)
        ),
      ),
      Text("")
    ],
  );
}

Widget weatherItems() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      at10(),
      at11(),
      at12(),
      at13(),
      at14(),
      at15(),
    ],
  );
}

Widget at10() {
  return Column(
    children: <Widget>[
      Text("10:00",
        style: TextStyle(
          color: Colors.white,
        ),
      ),
      IconButton(
        icon: Icon(
          Icons.sunny,
          color: Colors.yellow,
        ),
        onPressed: () {},
      ),
      Text("27°C",
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.w800,
        ),
      ),
    ],
  );
}

Widget at11() {
  return Column(
    children: <Widget>[
      Text("11:00",
        style: TextStyle(
          color: Colors.white,
        ),
      ),
      IconButton(
        icon: Icon(
          Icons.sunny,
          color: Colors.yellow,
        ),
        onPressed: () {},
      ),
      Text("29°C",
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.w800,
        ),
      ),
    ],
  );
}

Widget at12() {
  return Column(
    children: <Widget>[
      Text("12:00",
        style: TextStyle(
          color: Colors.white,
        ),
      ),
      IconButton(
        icon: Icon(
          Icons.sunny,
          color: Colors.yellow,
        ),
        onPressed: () {},
      ),
      Text("30°C",
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.w800,
        ),
      ),
    ],
  );
}

Widget at13() {
  return Column(
    children: <Widget>[
      Text("13:00",
        style: TextStyle(
          color: Colors.white,
        ),
      ),
      IconButton(
        icon: Icon(
          Icons.wb_cloudy_rounded,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text("30°C",
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.w800,
        ),
      ),
    ],
  );
}

Widget at14() {
  return Column(
    children: <Widget>[
      Text("14:00",
        style: TextStyle(
          color: Colors.white,
        ),
      ),
      IconButton(
        icon: Icon(
          Icons.wb_cloudy_rounded,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text("30°C",
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.w800,
        ),
      ),
    ],
  );
}

Widget at15() {
  return Column(
    children: <Widget>[
      Text("15:00",
        style: TextStyle(
          color: Colors.white,
        ),
      ),
      IconButton(
        icon: Icon(
          Icons.wb_cloudy_rounded,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text("30°C",
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.w800,
        ),
      ),
    ],
  );
}

Widget TodayListTile() {
  return ListTile(
    leading: Text("Today",
      style: TextStyle(
        color: Colors.white,
      ),
    ),
    title: IconButton(
      alignment: Alignment.centerRight,
      icon: Icon(Icons.sunny),
      color: Colors.yellow,
      onPressed: () {},
    ),
    trailing: Text("Mostly sunny  31/22°C",
      style: TextStyle(
        color: Colors.white,
      ),
    ),
  );
}

Widget TomorrowListTile() {
  return ListTile(
    leading: Text("Tomorrow",
      style: TextStyle(
        color: Colors.white,
      ),
    ),
    title: IconButton(
      alignment: Alignment.centerRight,
      icon: Icon(Icons.wb_cloudy_rounded),
      color: Colors.white,
      onPressed: () {},
    ),
    trailing: Text("Partly cloudy  31/22°C",
      style: TextStyle(
        color: Colors.white,
      ),
    ),
  );
}

Widget ThursdayListTile() {
  return ListTile(
    leading: Text("Thu",
      style: TextStyle(
        color: Colors.white,
      ),
    ),
    title: IconButton(
      alignment: Alignment.centerRight,
      icon: Icon(Icons.wb_cloudy_rounded),
      color: Colors.white,
      onPressed: () {},
    ),
    trailing: Text("       Cloudy       31/22°C",
      style: TextStyle(
        color: Colors.white,
      ),
    ),
  );
}

Widget FridayListTile() {
  return ListTile(
    leading: Text("Fri",
      style: TextStyle(
        color: Colors.white,
      ),
    ),
    title: IconButton(
      alignment: Alignment.centerRight,
      icon: Icon(Icons.cloud_queue_sharp),
      color: Colors.white,
      onPressed: () {},
    ),
    trailing: Text("   Overcast       29/21°C",
      style: TextStyle(
        color: Colors.white,
      ),
    ),
  );
}

Widget SaturdayListTile() {
  return ListTile(
    leading: Text("Sat",
      style: TextStyle(
        color: Colors.white,
      ),
    ),
    title: IconButton(
      alignment: Alignment.centerRight,
      icon: Icon(Icons.cloud_queue_sharp),
      color: Colors.white,
      onPressed: () {},
    ),
    trailing: Text("   Overcast       27/21°C",
      style: TextStyle(
        color: Colors.white,
      ),
    ),
  );
}

Widget SundayListTile() {
  return ListTile(
    leading: Text("Sun",
      style: TextStyle(
        color: Colors.white,
      ),
    ),
    title: IconButton(
      alignment: Alignment.centerRight,
      icon: Icon(Icons.wb_cloudy_rounded),
      color: Colors.white,
      onPressed: () {},
    ),
    trailing: Text("       Cloudy       29/22°C",

      style: TextStyle(
        color: Colors.white,
      ),
    ),
  );
}







